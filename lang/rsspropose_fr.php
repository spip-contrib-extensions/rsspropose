<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	
	'cfg_titre_parametrages' => 'Paramètres du plugin',
	'choixobjets' => 'choisir les objets à intégrer au flux RSS',
	
	// L
	
	'legende' =>'Clé de sécurité',
	
	// T
	
	'titre_page_configurer_rsspropose' => 'Un flux RSS pour suivre les validations',
	'token' => 'Indiquez la clé de votre choix',
	
	
);
